<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 10:44
 */

namespace VersusCode\core\DB;


use PDO;

class DBConnector
{
    private static $PDOInstance = null;
    private static $DBNAME = 'versus';
    private static $DBHOST = 'localhost';
    private static $DBUSER = 'root';
    private static $DBPWD = '';

    public static function getInstance()
    {
        if (self::$PDOInstance)
            return self::$PDOInstance;
        self::$PDOInstance = new PDO('mysql:dbname=' . self::$DBNAME . ';host=' . self::$DBHOST, self::$DBUSER, self::$DBPWD);
        self::$PDOInstance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        self::$PDOInstance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return self::$PDOInstance;
    }
}