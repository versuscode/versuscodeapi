<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 17-12-18
 * Time: 19:04
 */

namespace VersusCode\core\Routing;


use VersusCode\Controllers\BadgeController;
use VersusCode\Controllers\PartieController;
use VersusCode\Controllers\QuestionController;
use VersusCode\Controllers\TestController;
use VersusCode\Controllers\UserController;

class RouteGenerator
{
    public static function getAllRoutes()
    {
        return [
            new Route('/', 'GET', 'test', TestController::class),
            new Route('/users', 'GET', 'getAll', UserController::class),
            new Route('/users/:id', 'GET', 'get', UserController::class),
            new Route('/badges', 'GET', 'getAll', BadgeController::class),
            new Route('/badges/questions', 'POST', 'getAllByBadges', QuestionController::class),
            new Route('/badges/:id', 'GET', 'get', BadgeController::class),
            new Route('/badges/:id/questions', 'GET', 'getAllByBadge', QuestionController::class),
            new Route('/questions', 'GET', 'getAll', QuestionController::class),
            new Route('/questions/:id', 'GET', 'get', QuestionController::class),
            new Route('/parties', 'GET', 'getAll', PartieController::class),
            new Route('/parties', 'POST', 'add', PartieController::class),
            new Route('/parties/:id', 'GET', 'get', PartieController::class),
        ];
    }
}