<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 17-12-18
 * Time: 19:04
 */

namespace VersusCode\core\Routing;


use ReflectionClass;
use VersusCode\core\ErrorManagement\ErrorGenerator;

class Router
{
    private $routes = [];

    /**
     * @return Route[]
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @param array Route[]
     * @return Router
     */
    public function registerAllRoutes()
    {
        $this->routes = RouteGenerator::getAllRoutes();
        return $this;
    }

    /**
     * @param Route
     * @return Router
     */
    public function registerRoute($route)
    {
        $this->routes += $route;
        return $this;
    }

    public function handleRequest()
    {
        header("Content-Type: application/json; charset=utf-8");
        header("Access-Control-Allow-Origin: *");
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, OPTIONS');
            }
            if(isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }
            exit(0);
        }
        $uri = $_SERVER['REQUEST_URI'];
        $method = $_SERVER['REQUEST_METHOD'];
        $parse = $this->parseURL($uri);
        $route = $this->findRoute($parse['path'], $method);
        if (is_null($route))
        {
            http_response_code(404);
            return json_encode(ErrorGenerator::getError(404), JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }
        $route->addParams($parse['query-params']);
        $controller = null;
        $action = null;
        $reflector = null;
        try {
            $reflector = new ReflectionClass($route->getController());
            $controller = $reflector->newInstance();
            $action = $reflector->getMethod($route->getAction());
        } catch (\ReflectionException $e) {
        }
        $data = $controller->{$action->name}($route->getParams());
        return json_encode($data, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    private function findRoute($uri, $method)
    {
        foreach ($this->routes as $route)
        {
            /** @var Route $route */
            if (preg_match($route->getPathRegex(), $uri, $matches) === 1 && $method === $route->getMethod())
            {
                for ($i = 1; $i < count($matches); $i++)
                    $route->addParams([substr($route->getPathParams()[$i-1], 1, strlen($route->getPathParams()[$i-1])) => $matches[$i]]);
                return $route;
            }
        }
        return null;
    }

    private function parseURL($url)
    {
        $parse = parse_url($url);
        parse_str(parse_url($url, PHP_URL_QUERY),$parse['query-params']);
        return $parse;
    }
}