<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 17-12-18
 * Time: 19:03
 */

namespace VersusCode\core\Routing;


class Route
{
    /**
     * @var string
     */
    private $path;
    /**
     * @var mixed
     */
    private $params;
    /**
     * @var string
     */
    private $method;
    /**
     * @var string
     */
    private $action;
    /**
     * @var string
     */
    private $controller;
    /**
     * @var string
     */
    private $pathRegex;
    /**
     * @var array
     */
    private $pathParams;

    /**
     * Route constructor.
     * @param string $path
     * @param string $method
     * @param string $action
     * @param string $controller
     */
    public function __construct($path, $method, $action, $controller)
    {
        $this->path = $path;
        $this->method = $method;
        $this->action = $action;
        $this->controller = $controller;
        $this->params = [];
        $this->setPathParams();
        $this->setPathRegex();
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return Route
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param mixed $params
     * @return Route
     */
    public function setParams($params)
    {
        $this->params = $params;
        return $this;
    }

    /**
     * @param array $params
     * @return mixed|null
     */
    public function addParams($params)
    {
        foreach ($params as $key => $value)
            $this->params[$key] = $value;
        return $this->params;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return Route
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return Route
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     * @return Route
     */
    public function setController($controller)
    {
        $this->controller = $controller;
        return $this;
    }

    /**
     * @return string
     */
    public function getPathRegex()
    {
        return $this->pathRegex;
    }

    /**
     * @return Route
     */
    private function setPathRegex()
    {
        $regex = '#^' . $this->path . '/?$#';
        foreach ($this->pathParams as $param)
            $regex = str_replace($param, '(\w+)', $regex);
        $regex = str_replace('/', '\/', $regex);
        $this->pathRegex = $regex;
        return $this;
    }

    /**
     * @return array
     */
    public function getPathParams()
    {
        return $this->pathParams;
    }

    /**
     * @return Route
     */
    private function setPathParams()
    {
        $parse = $this->parseURL($this->path);
        if (array_key_exists('path-params', $parse))
            $this->pathParams = $parse['path-params'];
        else
            $this->pathParams = [];
        return $this;
    }

    private function parseURL($url)
    {
        $parse = parse_url($url);
        parse_str(parse_url($url, PHP_URL_QUERY), $parse['query-params']);
        $array = array_filter(explode('/', parse_url($url, PHP_URL_PATH)), function ($value) {
            return (strpos($value, ':') !== false);
        });
        foreach ($array as $item)
            $parse['path-params'][] = $item;
        return $parse;
    }
}