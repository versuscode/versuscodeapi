<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 16:12
 */

namespace VersusCode\core\UTF8Management;

class UTF8Encoder
{
    /**
     * Recursive function to encode any string to UTF-8,
     * to make up for the latin accents problem,
     * @param $data , which is an array or a scalar (boolean/number/string)
     * @return array|string, which will be the result of the conversion.
     * Nested objects are returned without treatment because their properties
     * will be treated by an encode call in their own jsonSerialize() call.
     */
    public static function encode($data)
    {
        if (is_scalar($data)) {
            // If data is scalar (boolean/number/string), return data encoded.
            return utf8_encode($data);
        } elseif (is_array($data)) {
            // If data is array, call encode on each property,
            // and assign the result on the corresponding key.
            foreach ($data as $key => $entry) {
                $data[$key] = self::encode($entry);
            }
            return $data;
        } else
            // If data is object, return untouched data for future treatment
            return $data;
    }
}