<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 30-11-18
 * Time: 11:40
 */

namespace VersusCode\core\ErrorManagement;


use JsonSerializable;

class Error implements JsonSerializable
{
    /**
     * @var int $code
     */
    private $code;

    /**
     * @var string message
     */
    private $message;

    public function __construct($code, $message)
    {
        $this->code = $code;
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return Error
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Error
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

}