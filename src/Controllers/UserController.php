<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 11:54
 */

namespace VersusCode\Controllers;


use Toolbox\BaseController;
use VersusCode\Repositories\UserRepository;

class UserController extends BaseController
{
    public function getRepositoryName()
    {
        return UserRepository::class;
    }
}