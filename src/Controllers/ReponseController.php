<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 13:59
 */

namespace VersusCode\Controllers;


use Toolbox\BaseController;
use VersusCode\Repositories\ReponseRepository;

class ReponseController extends BaseController
{

    public function getRepositoryName()
    {
       return ReponseRepository::class;
    }
}