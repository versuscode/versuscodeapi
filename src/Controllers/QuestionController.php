<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 12:27
 */

namespace VersusCode\Controllers;


use Toolbox\BaseController;
use VersusCode\Repositories\QuestionRepository;

class QuestionController extends BaseController
{

    public function getRepositoryName()
    {
        return QuestionRepository::class;
    }

    public function getAllByBadge($params = null)
    {
        if (array_key_exists('id', $params))
            return $this->repo->getAllByBadge($params['id']);
        else
            return $this->repo->getAllByBadge(0);
    }

    public function getAllByBadges()
    {
        $questions = [];
        foreach ($_POST as $id)
            $questions = array_merge($questions, $this->repo->getAllByBadge($id));
        return $questions;
    }
}