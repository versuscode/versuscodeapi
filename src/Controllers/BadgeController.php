<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 12:19
 */

namespace VersusCode\Controllers;


use Toolbox\BaseController;
use VersusCode\Repositories\BadgeRepository;

class BadgeController extends BaseController
{
    public function getRepositoryName()
    {
        return BadgeRepository::class;
    }
}