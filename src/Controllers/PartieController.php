<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 04-01-19
 * Time: 12:57
 */

namespace VersusCode\Controllers;


use Toolbox\BaseController;
use VersusCode\Repositories\PartieRepository;

class PartieController extends BaseController
{

    public function getRepositoryName()
    {
        return PartieRepository::class;
    }
}