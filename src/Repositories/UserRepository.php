<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 11:57
 */

namespace VersusCode\Repositories;


use Toolbox\BaseRepository;
use VersusCode\Models\User;

class UserRepository extends BaseRepository
{

    public function getTableName()
    {
        return 'users';
    }

    public function getEntityName()
    {
        return User::Class;
    }

    public function getPKBindings()
    {
        return [
            'id' => 'id',
        ];
    }

    public function getBindings()
    {
       return [
           'pseudo' => 'pseudo',
           'level' => 'lvl',
       ];
    }
}