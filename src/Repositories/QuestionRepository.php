<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 12:24
 */

namespace VersusCode\Repositories;


use PDO;
use Toolbox\BaseRepository;
use VersusCode\Models\Question;

class QuestionRepository extends BaseRepository
{

    public function getTableName()
    {
        return 'questions';
    }

    public function getEntityName()
    {
        return Question::class;
    }

    public function getPKBindings()
    {
        return [
            'id' => 'id',
        ];
    }

    public function getBindings()
    {
        return [
            'intitule' => 'question',
            'badgeId' => 'badgeID',
            'bonneReponseId' => 'reponseID',
        ];
    }

    public function getAllByBadge($badgeId)
    {
        $query = 'SELECT ';
        foreach (array_merge($this->getPKBindings(), $this->getBindings()) as $phpVariableName => $dbColumnName)
        {
            $query .= $dbColumnName;
            $query .= ' AS ';
            $query .= $phpVariableName;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM ';
        $query .= $this->getTableName();
        $query .= ' WHERE badgeID = :badgeId';
        $stmt = $this->pdo->prepare($query);
        $tab['badgeId'] = $badgeId;
        $stmt->execute($tab);
        $stmt->setFetchMode(PDO::FETCH_CLASS, $this->getEntityName());
        $entities = $stmt->fetchAll();
        return $entities;
    }
}