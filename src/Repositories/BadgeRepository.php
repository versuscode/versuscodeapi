<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 12:20
 */

namespace VersusCode\Repositories;


use PDO;
use Toolbox\BaseRepository;
use VersusCode\Models\Badge;

class BadgeRepository extends BaseRepository
{

    public function getTableName()
    {
        return 'badges';
    }

    public function getEntityName()
    {
        return Badge::Class;
    }

    public function getPKBindings()
    {
        return [
            'id' => 'id',
        ];
    }

    public function getBindings()
    {
        return [
            'name' => 'nom',
        ];
    }

    public function getBadgesByUser($userId)
    {
        $tab = [];
        $query = 'SELECT ';
        foreach (array_merge($this->getPKBindings(), $this->getBindings()) as $phpVariableName => $dbColumnName) {
            $query .= $dbColumnName;
            $query .= ' AS ';
            $query .= $phpVariableName;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM badgesbyuser WHERE userID = :userId';
        $tab[':userId'] = $userId;
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($tab);
        $stmt->setFetchMode(PDO::FETCH_CLASS, $this->getEntityName());
        $entities = $stmt->fetchAll();
        return $entities;
    }
}