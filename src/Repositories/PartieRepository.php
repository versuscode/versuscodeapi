<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 04-01-19
 * Time: 12:47
 */

namespace VersusCode\Repositories;


use Toolbox\BaseRepository;
use VersusCode\Models\Partie;

class PartieRepository extends BaseRepository
{

    public function getTableName()
    {
        return 'parties';
    }

    public function getEntityName()
    {
        return Partie::class;
    }

    public function getPKBindings()
    {
        return [
            'id' => 'id',
        ];
    }

    public function getBindings()
    {
        return [
            'player1Id' => 'joueurUn',
            'player2Id' => 'joueurDeux',
            'winnerId' => 'gagnant',
        ];
    }
}