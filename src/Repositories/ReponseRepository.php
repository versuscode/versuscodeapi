<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 13:54
 */

namespace VersusCode\Repositories;


use PDO;
use Toolbox\BaseRepository;
use VersusCode\Models\Reponse;

class ReponseRepository extends BaseRepository
{

    public function getTableName()
    {
        return 'reponses';
    }

    public function getEntityName()
    {
        return Reponse::class;
    }

    public function getPKBindings()
    {
        return [
            'id' => 'id',
        ];
    }

    public function getBindings()
    {
        return [
            'contenu' => 'reponse',
        ];
    }

    public function getWrongsByQuestion($questionId)
    {
        $tab = [];
        $query = 'SELECT ';
        foreach (array_merge($this->getPKBindings(), $this->getBindings()) as $phpVariableName => $dbColumnName) {
            $query .= $dbColumnName;
            $query .= ' AS ';
            $query .= $phpVariableName;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM mauvaisesreponses WHERE questionID = :questionId';
        $tab[':questionId'] = $questionId;
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($tab);
        $stmt->setFetchMode(PDO::FETCH_CLASS, $this->getEntityName());
        $entities = $stmt->fetchAll();
        return $entities;
    }
}