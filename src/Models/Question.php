<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 12:23
 */

namespace VersusCode\Models;


use Toolbox\BaseEntity;
use VersusCode\core\UTF8Management\UTF8Encoder;
use VersusCode\Repositories\BadgeRepository;
use VersusCode\Repositories\ReponseRepository;

class Question extends BaseEntity
{
    private $id;
    private $intitule;
    private $badgeId;
    private $badge;
    private $bonneReponseId;
    private $bonneReponse;
    private $mauvaisesReponses;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Question
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * @param mixed $intitule
     * @return Question
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBadgeId()
    {
        return $this->badgeId;
    }

    /**
     * @param mixed $badgeId
     * @return Question
     */
    public function setBadgeId($badgeId)
    {
        $this->badgeId = $badgeId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBadge()
    {
        if ($this->badge === null)
        {
            $repo = new BadgeRepository();
            $this->badge = $repo->get($this->badgeId);
        }
        return $this->badge;
    }

    /**
     * @param mixed $badge
     * @return Question
     */
    public function setBadge($badge)
    {
        $this->badge = $badge;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBonneReponseId()
    {
        return $this->bonneReponseId;
    }

    /**
     * @param mixed $bonneReponseId
     * @return Question
     */
    public function setBonneReponseId($bonneReponseId)
    {
        $this->bonneReponseId = $bonneReponseId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBonneReponse()
    {
        if ($this->bonneReponse === null)
        {
            $repo = new ReponseRepository();
            $this->bonneReponse = $repo->get($this->bonneReponseId);
        }
        return $this->bonneReponse;
    }

    /**
     * @param mixed $bonneReponse
     * @return Question
     */
    public function setBonneReponse($bonneReponse)
    {
        $this->bonneReponse = $bonneReponse;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMauvaisesReponses()
    {
        if ($this->mauvaisesReponses === null)
        {
            $repo = new ReponseRepository();
            $this->mauvaisesReponses = $repo->getWrongsByQuestion($this->id);
        }
        return $this->mauvaisesReponses;
    }

    /**
     * @param mixed $mauvaisesReponses
     */
    public function setMauvaisesReponses($mauvaisesReponses)
    {
        $this->mauvaisesReponses = $mauvaisesReponses;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        foreach ($this->getJSONEncode() as $JSONEncode)
            $this->{'get' . ucwords($JSONEncode)}();
        $properties = get_object_vars($this);
        foreach ($this->getJSONIgnore() as $JSONIgnore)
            unset($properties[$JSONIgnore]);
        return UTF8Encoder::encode($properties);
    }

    public function getJSONIgnore()
    {
        return [
            'badgeId',
            'reponseId',
            'utf8Encoder',
        ];
    }

    public function getJSONEncode()
    {
        return [
            'badge',
            'bonneReponse',
            'mauvaisesReponses',
        ];
    }

    public function getMapping($params)
    {
        foreach ($params as $key => $param)
            $this->{$key} = $param;
        return $this;
    }
}