<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 11:52
 */

namespace VersusCode\Models;


use Toolbox\BaseEntity;
use VersusCode\core\UTF8Management\UTF8Encoder;
use VersusCode\Repositories\BadgeRepository;

class User extends BaseEntity
{
    private $id;
    private $pseudo;
    private $badges;
    private $level;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * @param mixed $pseudo
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;
    }

    /**
     * @return mixed
     */
    public function getBadges()
    {
        if ($this->badges === null)
        {
            $repo = new BadgeRepository();
            $this->badges = $repo->getBadgesByUser($this->id);
        }
        return $this->badges;
    }

    /**
     * @param mixed $badges
     */
    public function setBadges($badges)
    {
        $this->badges = $badges;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        foreach ($this->getJSONEncode() as $JSONEncode)
            $this->{'get' . ucwords($JSONEncode)}();
        $properties = get_object_vars($this);
        foreach ($this->getJSONIgnore() as $JSONIgnore)
            unset($properties[$JSONIgnore]);
        return UTF8Encoder::encode($properties);
    }

    public function getJSONIgnore()
    {
        return [
            'utf8Encoder',
        ];
    }

    public function getJSONEncode()
    {
        return [
            'badges',
        ];
    }

    public function getMapping($params)
    {
        foreach ($params as $key => $param)
            $this->{$key} = $param;
        return $this;
    }
}