<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 12:20
 */

namespace VersusCode\Models;


use Toolbox\BaseEntity;
use VersusCode\core\UTF8Management\UTF8Encoder;

class Badge extends BaseEntity
{
    private $id;
    private $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Badge
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Badge
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        $properties = get_object_vars($this);
        foreach ($this->getJSONIgnore() as $JSONIgnore)
            unset($properties[$JSONIgnore]);
        return UTF8Encoder::encode($properties);
    }

    public function getJSONIgnore()
    {
        return [
            'utf8Encoder',
        ];
    }

    public function getJSONEncode()
    {
        // TODO: Implement getJSONEncode() method.
    }

    public function getMapping($params)
    {
        foreach ($params as $key => $param)
            $this->{$key} = $param;
        return $this;
    }
}