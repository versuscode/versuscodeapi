<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 04-01-19
 * Time: 12:30
 */

namespace VersusCode\Models;


use Toolbox\BaseEntity;
use VersusCode\core\UTF8Management\UTF8Encoder;
use VersusCode\Repositories\UserRepository;

class Partie extends BaseEntity
{
    private $id;
    private $player1Id;
    private $player2Id;
    private $winnerId;
    private $player1;
    private $player2;
    private $winner;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Partie
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayer1Id()
    {
        return $this->player1Id;
    }

    /**
     * @param mixed $player1Id
     */
    public function setPlayer1Id($player1Id)
    {
        $this->player1Id = $player1Id;
    }

    /**
     * @return mixed
     */
    public function getPlayer2Id()
    {
        return $this->player2Id;
    }

    /**
     * @param mixed $player2Id
     */
    public function setPlayer2Id($player2Id)
    {
        $this->player2Id = $player2Id;
    }

    /**
     * @return mixed
     */
    public function getWinnerId()
    {
        return $this->winnerId;
    }

    /**
     * @param mixed $winnerId
     */
    public function setWinnerId($winnerId)
    {
        $this->winnerId = $winnerId;
    }

    /**
     * @return mixed
     */
    public function getPlayer1()
    {
        if ($this->player1 == null)
        {
            $repo = new UserRepository();
            $this->player1 = $repo->get($this->player1Id);
        }
        return $this->player1;
    }

    /**
     * @param mixed $player1
     * @return Partie
     */
    public function setPlayer1($player1)
    {
        $this->player1 = $player1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlayer2()
    {
        if ($this->player2 == null)
        {
            $repo = new UserRepository();
            $this->player2 = $repo->get($this->player2Id);
        }
        return $this->player2;
    }

    /**
     * @param mixed $player2
     * @return Partie
     */
    public function setPlayer2($player2)
    {
        $this->player2 = $player2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWinner()
    {
        if ($this->winner == null)
        {
            $repo = new UserRepository();
            $this->winner = $repo->get($this->winnerId);
        }
        return $this->winner;
    }

    /**
     * @param mixed $winner
     * @return Partie
     */
    public function setWinner($winner)
    {
        $this->winner = $winner;
        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        foreach ($this->getJSONEncode() as $JSONEncode)
            $this->{'get' . ucwords($JSONEncode)}();
        $properties = get_object_vars($this);
        foreach ($this->getJSONIgnore() as $JSONIgnore)
            unset($properties[$JSONIgnore]);
        return UTF8Encoder::encode($properties);
    }

    /**
     * Specify properties which won't be encoded to JSON,
     * @return array of property names.
     */
    public function getJSONIgnore()
    {
        return [
            'player1Id',
            'player2Id',
            'winnerId',
            'utf8Encoder',
        ];
    }

    /**
     * Specify properties which will be encoded to JSON
     * that don't exist yet in the current context, typically
     * properties linked to a foreign key,
     * @return array of property names.
     */
    public function getJSONEncode()
    {
        return [
            'player1',
            'player2',
            'winner',
        ];
    }

    public function getMapping($params)
    {
        foreach ($params as $key => $param)
            $this->{$key} = $param;
        return $this;
    }
}