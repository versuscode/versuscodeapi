<?php

namespace Toolbox;

use PDO;
use VersusCode\core\DB\DBConnector;

/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 10:50
 */

abstract class BaseRepository
{
    /** @var PDO $pdo */
    protected $pdo;

    public function __construct()
    {
        $this->pdo = DBConnector::getInstance();
    }

    public abstract function getTableName();
    public abstract function getEntityName();
    public abstract function getPKBindings();
    public abstract function getBindings();

    function getAll($params = null)
    {
        $params = (is_null($params)) ? [] : $params;
        $query = 'SELECT ';
        foreach (array_merge($this->getPKBindings(), $this->getBindings()) as $phpVariableName => $dbColumnName)
        {
            $query .= $dbColumnName;
            $query .= ' AS ';
            $query .= $phpVariableName;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM ';
        $query .= $this->getTableName();
        if (array_key_exists('limit', $params))
        {
            $query .= ' LIMIT ';
            $query .= $params['limit'];
            if (array_key_exists('offset', $params))
            {
                $query .= ' OFFSET ';
                $query .= $params['offset'];
            }
        }
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, $this->getEntityName());
        $entities = $stmt->fetchAll();
        return $entities;
    }

    function get($id)
    {
        $tab = [];
        $query = 'SELECT ';
        foreach (array_merge($this->getPKBindings(), $this->getBindings()) as $phpVariableName => $dbColumnName)
        {
            $query .= $dbColumnName;
            $query .= ' AS ';
            $query .= $phpVariableName;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ' FROM ';
        $query .= $this->getTableName();
        $query .= ' WHERE ';
        foreach ($this->getPKBindings() as $phpVariableName => $dbColumnName)
        {
            $query .= $dbColumnName;
            $query .= ' = :';
            $query .= $phpVariableName;
            $query .= ' AND ';
            $tab[':' . $phpVariableName] = $id;
        }
        $query = substr($query, 0, -5);
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($tab);
        $stmt->setFetchMode(PDO::FETCH_CLASS, $this->getEntityName());
        $entity = $stmt->fetch();
        return $entity;
    }

    function post($entity)
    {
        $tab = [];
        $query = 'INSERT INTO ';
        $query .= $this->getTableName();
        $query .= ' (';
        foreach ($this->getBindings() as $phpVariableName => $dbColumnName)
        {
            $query .= $dbColumnName;
            $query .= ', ';
        }
        $query = substr($query, 0, -2);
        $query .= ') VALUES (:';
        foreach ($this->getBindings() as $phpVariableName => $dbColumnName)
        {
            $query .= $phpVariableName . ', :';
            $tab[$phpVariableName] = $entity->{"get".ucwords($phpVariableName)}();
        }
        $query = substr($query, 0, -3);
        $query .= ')';
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($tab);
        return $this->pdo->lastInsertId();
    }

    function put($id, $entity)
    {
        $tab = [];
        $query = 'UPDATE ';
        $query .= $this->getTableName();
        $query .= ' SET ';
        foreach ($this->getBindings() as $phpVariableName => $dbColumnName)
            if ($entity->{"get" . ucwords($phpVariableName)}() !== null)
            {
                $query .= $dbColumnName . ' = :' . $phpVariableName . ', ';
                $tab[':' . $phpVariableName] = $entity->{"get" . ucwords($phpVariableName)}();
            }
        $query = substr($query, 0, -2);
        $query .= ' WHERE ';
        foreach ($this->getPKBindings() as $phpVariableName => $dbColumnName)
        {
            $query .= $dbColumnName . ' = :' . $phpVariableName .  ' AND ';
            $tab[':' . $phpVariableName] = $id;
        }
        $query = substr($query, 0, -5);
        $stmt = $this->pdo->prepare($query);
        return $stmt->execute($tab);
    }

    function delete($id)
    {
        $tab = [];
        $query = 'DELETE FROM ';
        $query .= $this->getTableName();
        $query .= ' WHERE ';
        foreach ($this->getPKBindings() as $phpVariableName => $dbColumnName)
        {
            $query .= $dbColumnName;
            $query .= ' = :';
            $query .= $phpVariableName;
            $query .= ' AND ';
            $tab[':' . $phpVariableName] = $id;
        }
        $query = substr($query, 0, -5);
        $stmt = $this->pdo->prepare($query);
        return $stmt->execute($tab);
    }
}