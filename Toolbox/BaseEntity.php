<?php
/**
 * Created by PhpStorm.
 * User: studentsc03
 * Date: 03-01-19
 * Time: 11:13
 */

namespace Toolbox;


use JsonSerializable;
use VersusCode\core\UTF8Management\UTF8Encoder;

abstract class BaseEntity implements JsonSerializable
{
    protected $utf8Encoder;

    public function __construct()
    {
        $this->utf8Encoder = new UTF8Encoder();
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public abstract function jsonSerialize();

    /**
     * Specify properties which won't be encoded to JSON,
     * @return array of property names.
     */
    public abstract function getJSONIgnore();

    /**
     * Specify properties which will be encoded to JSON
     * that don't exist yet in the current context, typically
     * properties linked to a foreign key,
     * @return array of property names.
     */
    public abstract function getJSONEncode();

    public abstract function getMapping($params);
}